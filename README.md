# README #


### Automarket ###

* Potrebno je kreirati Android aplikaciju za potrebe pregleda automobila na trzistu.
* Svaki od korisnika treba da kreira svoj nalog, zatim da se uloguje sa kreiranim nalogom. 
* Kada je korisnik ulogovan otvara se ponuda automobila koji se trenutno nalaze na trzistu. 
* Korisnik moze da otvori detalje automobila. 
* Takodje korisniku treba pruziti opciju da se izloguje u bilo kom trenutku. 
* Samo ulogovani korisnici imaju opciju pregleda marketa.

Link do API dokumentacije: [link](http://esokia-api.ddns.net:5000/swagger "Documentation").


## ZADACI ##

### 1. Registracija ###

Korisnik pri registraciji unosi sledece parametre:

+ Email
+ Password

### 2. Prijava korisnika (Login) ###

Korisnik pri login-u unosi sledece parametre:

+ Email
+ Password

### 3. Odjava korisnika (Logout) ###

Korisnik pri logout-u unosi sledece parametre:

+ Id

### 4. Pregled automobila ###

Potrebno je prikazati listu automobila sa sledecim atributima:
+ Proizvodjac
+ Model
+ Slika 

### 5. Pregled detalja automobila ###

Prilikom otvaranja detalja automobila potrebno koristiti Android toolbar. Kao naslov prikazati marku-model automobila, toolbar obojiti u odgovarajucu boju i kao akciju iz toolbara pruziti korisniku mogucnost da se izloguje. Sve ostale informacije o automobilu je potrebno prikazati na istom ekranu.

**Napomena:** Pozeljno je sortirati automobile po godini proizvodnje.


### Uputstvo za git ###

Pre pocetka rada potrebno je krerati zasebnu granu (**ime.prezime**) i po zavrsetku test projekta neophodno je odraditi push projekta na kreiranu granu, a potom i pull request. Nikako ne mergeovati kreiranu granu sa **master** granom.


Kontakti za slucaj nejasnoca:

1. Stefan Spasic: ss@esokia.com
2. bojan Kostic: bojan@esokia.com